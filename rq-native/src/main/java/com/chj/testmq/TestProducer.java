package com.chj.testmq;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * direct类型交换器的生产者
 */
public class TestProducer {
    public final static String EXCHANGE_NAME = "direct_logs";
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("10.200.169.5");
        connectionFactory.setVirtualHost("chj_vhost");
        connectionFactory.setUsername("chj");
        connectionFactory.setPassword("123456");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 创建交换器 BuiltinExchangeType.DIRECT == direct
//        channel.exchangeDeclare(EXCHANGE_NAME,"direct");
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        //日志消息级别，作为路由键使用
        String[] routekeys = {"hankin01","hankin02","hankin03"};
        for(int i=0;i<3;i++) {
            String routekey = routekeys[i % 3];
            String msg = "Hello, RabbitMQ" + (i + 1);
            //发布消息，需要参数：交换器，路由键，其中以日志消息级别为路由键
            channel.basicPublish(EXCHANGE_NAME,routekey,null,msg.getBytes());
            System.out.println("Send "+routekey+":"+msg);
        }
        channel.close();
        connection.close();
    }

}
