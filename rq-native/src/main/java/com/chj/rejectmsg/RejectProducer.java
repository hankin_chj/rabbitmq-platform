package com.chj.rejectmsg;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 存放到延迟队列的元素，对业务数据进行了包装
 */
public class RejectProducer {
    public final static String EXCHANGE_NAME = "direct_logs";
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("10.200.169.5");
        connectionFactory.setVirtualHost("chj_vhost");
        connectionFactory.setUsername("chj");
        connectionFactory.setPassword("123456");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        // 创建交换器
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        //发送10条消息
        for(int i=0;i<10;i++){
            String message = "Hello World_"+(i+1);
            // 参数1：exchange name  参数2：routing key
            channel.basicPublish(EXCHANGE_NAME, "error",null, message.getBytes());
            System.out.println(" [x] Sent 'error':'"+ message + "'");
        }
        // 关闭频道和连接
        channel.close();
        connection.close();
    }
}
