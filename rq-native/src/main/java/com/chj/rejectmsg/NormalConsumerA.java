package com.chj.rejectmsg;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 普通的消费者
 */
public class NormalConsumerA {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("10.200.169.5");
        connectionFactory.setVirtualHost("chj_vhost");
        connectionFactory.setUsername("chj");
        connectionFactory.setPassword("123456");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        final Channel channel = connection.createChannel();
        // 创建交换器
        channel.exchangeDeclare(RejectProducer.EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 声明一个队列
        String queueName = "focuserror";
        channel.queueDeclare(queueName,false,false,false,null);

        // 绑定，将队列和交换器通过路由键进行绑定
        String routekey = "error";/*表示只关注error级别的日志消息*/
        channel.queueBind(queueName,RejectProducer.EXCHANGE_NAME,routekey);
        System.out.println("waiting for message........");
        // 声明了一个消费者
        final Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received1["+envelope.getRoutingKey() +"]"+message);
                //todo 消息确认
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };
        // 消费者正式开始在指定队列上消费消息
        channel.basicConsume(queueName,false,consumer);

    }
}
