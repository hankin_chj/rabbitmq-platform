package com.chj.rejectmsg;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 拒绝消息的消费者
 */
public class RejectRequeuConsumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("10.200.169.5");
        connectionFactory.setVirtualHost("chj_vhost");
        connectionFactory.setUsername("chj");
        connectionFactory.setPassword("123456");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        final Channel channel = connection.createChannel();
        // 创建交换器
        channel.exchangeDeclare(RejectProducer.EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 声明一个队列
        String queueName = "focuserror";
        channel.queueDeclare(queueName,false,false,false,null);

        // 绑定，将队列和交换器通过路由键进行绑定
        String routekey = "error";/*表示只关注error级别的日志消息*/
        channel.queueBind(queueName,RejectProducer.EXCHANGE_NAME,routekey);
        System.out.println("waiting for message........");
        // 声明了一个消费者
        final Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                try{
                    String message = new String(body, "UTF-8");
                    System.out.println("Received3[" +envelope.getRoutingKey()+"]"+message);
                    throw new RuntimeException("处理异常"+message);
                }catch (Exception e){
                    e.printStackTrace();
                    //TODO Reject方式拒绝(这里第2个参数决定是否重新投递)
                    channel.basicReject(envelope.getDeliveryTag(),true);
                    //TODO Nack方式的拒绝（第2个参数决定是否批量）
//                    channel.basicNack(envelope.getDeliveryTag(), false, true);
                }
            }
        };
        // 消费者正式开始在指定队列上消费消息
        channel.basicConsume(queueName,false,consumer);
    }
}
