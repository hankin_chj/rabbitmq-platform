package com.chj.dlx;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 生产消息
 */
public class DlxProducer {
    public final static String EXCHANGE_NAME = "dlx_make";
    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建连接连接到MabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.200.169.5");
        factory.setVirtualHost("chj_vhost");
        factory.setUsername("chj");
        factory.setPassword("123456");
        // 创建一个连接
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // 指定转发
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        // 路由键使用
        String[] routekeys = {"king","mark","james"};
        for(int i=0;i<3;i++){
            String routekey = routekeys[i%3];
            String msg = "Hellol,RabbitMq"+(i+1);
            // 发布消息，需要参数：交换器，路由键，其中以日志消息级别为路由键
            channel.basicPublish(EXCHANGE_NAME,routekey,null,msg.getBytes());
            System.out.println("Sent "+routekey+":"+msg);
        }
        // 关闭频道和连接
        channel.close();
        connection.close();
    }
}
