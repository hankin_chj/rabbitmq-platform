package com.chj.consumer_balance.ackfalse;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 普通生产者
 */
public class AckFalseProducer {
    public final static String EXCHANGE_NAME = "direct_logs";
    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("10.200.169.5");
        connectionFactory.setVirtualHost("chj_vhost");
        connectionFactory.setUsername("chj");
        connectionFactory.setPassword("123456");
        // 创建连接
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();  // 创建信道
        // 创建交换器 BuiltinExchangeType.DIRECT == direct
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // TODO 启用异步通知发送者确认模式
        channel.confirmSelect();
        //日志消息级别，作为路由键使用
        for(int i=0;i<3;i++) {
            String message = "Hello World_"+(i+1);
            // 参数1：exchange name  参数2：routing key
            channel.basicPublish(EXCHANGE_NAME,"error",null,message.getBytes());
            System.out.println(" [x] Sent 'error':'" + message + "'");
            // TODO 确认是否成功
            if(channel.waitForConfirms()){
                System.out.println("send success");
            }else{
                System.out.println("send failure");
            }
        }
        // 关闭频道和连接
        channel.close();
        connection.close();
    }
}
