package com.chj.exchange.fanout;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * fanout生产者
 */
public class FanoutProducer {
    public final static String EXCHANGE_NAME = "fanout_logs";
    public static void main(String[] args)throws IOException, TimeoutException {
        // 创建连接连接到RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.200.169.5");
        factory.setVirtualHost("chj_vhost");
        factory.setUsername("chj");
        factory.setPassword("123456");
        // 创建一个连接
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // TODO 指定转发 FANOUT 交换器
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
//        String queueName = "producer_create";
//        channel.queueDeclare(queueName,false,false,false,null);
//        channel.queueBind(queueName,EXCHANGE_NAME,"test");
        // 路由键设置
        String[] routekeys = {"chj01","chj02","chj03"};
        for(int i=0;i<3;i++){
            String routekey = routekeys[i%3];//每一次发送一条消息
            // 发送的消息
            String message = "Hello World_"+(i+1);
            //参数1：exchange name 参数2：routing key
            channel.basicPublish(EXCHANGE_NAME, routekey,null, message.getBytes());
            System.out.println(" [x] Sent '" + routekey +"':'" + message + "'");
        }
        // 关闭频道和连接
        channel.close();
        connection.close();
    }
}
