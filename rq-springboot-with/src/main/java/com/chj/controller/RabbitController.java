package com.chj.controller;

import com.chj.fanout.FanoutSender;
import com.chj.hello.DefaultSender;
import com.chj.topic.TopicSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *类说明： localhost:8080/rabbit/hello
 */
@RestController
@RequestMapping("/rabbit")
public class RabbitController {
    @Autowired
    private DefaultSender defaultSender;
    @Autowired
    private TopicSender topicSender;
    @Autowired
    private FanoutSender fanoutSender;

    /**
     * 普通类型测试
     */
    @GetMapping("/hello")
    public void hello(){ //mq的消息发送
        String message = "hello rabbitmq!!";
        defaultSender.send(message);
    }

    /**
     * topic exchange类型rabbitmq测试
     */
    @GetMapping("/topicTest")
    public void topicTest() {
        topicSender.send();
    }

    /**
     * fanout exchange类型rabbitmq测试
     */
    @GetMapping("/fanoutTest")
    public void fanoutTest() {
        fanoutSender.send("hellomsg:OK");
    }

}
