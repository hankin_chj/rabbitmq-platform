package com.chj.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "hankin.info.user")
public class TopicUserMessageReceiver {
    @RabbitHandler
    public void process(String message){
        System.out.println("TopicUser Message Receiver  : " +message);
    }
}
