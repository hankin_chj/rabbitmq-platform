package com.chj.util;
/**

 *类说明：
 */
public class RmConst {

    public final static String QUEUE_HELLO = "hankin.hello";
    public final static String QUEUE_USER = "hankin.user";

    public final static String QUEUE_TOPIC_EMAIL = "hankin.info.email";
    public final static String QUEUE_TOPIC_USER = "hankin.info.user";
    public final static String RK_EMAIL = "hankin.info.email";
    public final static String RK_USER = "hankin.info.user";

    public final static String EXCHANGE_TOPIC = "hankin.exchange";
    public final static String EXCHANGE_FANOUT = "hankin.fanout.exchange";




}
